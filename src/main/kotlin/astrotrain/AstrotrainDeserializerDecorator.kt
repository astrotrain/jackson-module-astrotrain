package astrotrain

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.ResolvableDeserializer
import com.fasterxml.jackson.databind.node.TreeTraversingParser

internal class AstrotrainDeserializerDecorator(
        private val adapters: List<(JsonNode)->Unit>,
        private val defaultDeserializer: JsonDeserializer<*>
) : JsonDeserializer<Any>(), ResolvableDeserializer {

    override fun resolve(ctxt: DeserializationContext?) {
        if (defaultDeserializer is ResolvableDeserializer) {
            defaultDeserializer.resolve(ctxt)
        }
    }

    override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Any {
        val jsonNode = ctxt.readValue<JsonNode>(p, JsonNode::class.java)

        adapters.forEach {
            it(jsonNode)
        }

        val newParser = TreeTraversingParser(jsonNode)
        // Not sure why we need to do this but the deserializer code is expecting the current node to be the start object
        newParser.nextToken()
        return defaultDeserializer.deserialize(newParser, ctxt)
    }
}