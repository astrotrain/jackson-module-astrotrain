package astrotrain

import com.fasterxml.jackson.databind.BeanDescription
import com.fasterxml.jackson.databind.DeserializationConfig
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier

internal class AstrotrainBeanDeserializerModifier(private val adapters: List<Pair<Class<*>, (JsonNode)->Unit>>) : BeanDeserializerModifier() {
    override fun modifyDeserializer(
            config: DeserializationConfig,
            beanDesc: BeanDescription,
            deserializer: JsonDeserializer<*>
    ): JsonDeserializer<out Any> {
        val modifiedFromParent = super.modifyDeserializer(config, beanDesc, deserializer)

        val adapters = adapters.filter {
            it.first == beanDesc.beanClass
        }. map {
            it.second
        }

        return if (adapters.isNotEmpty()) {
            AstrotrainDeserializerDecorator(adapters, modifiedFromParent)
        } else {
            modifiedFromParent
        }
    }
}