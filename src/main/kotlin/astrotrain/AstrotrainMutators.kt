package astrotrain

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode

fun renameField(originalFieldName: String, newFieldName: String) : (JsonNode) -> Unit {
    return { jsonNode ->
        val objectNode = jsonNode as ObjectNode
        if (objectNode.has(originalFieldName)) {
            val fieldValue : JsonNode = objectNode.get(originalFieldName)
            objectNode.remove(originalFieldName)
            objectNode.replace(newFieldName, fieldValue)
        }
    }
}

fun removeField(fieldName: String) : (JsonNode) -> Unit {
    return { jsonNode ->
        val objectNode = jsonNode as ObjectNode
        if (objectNode.has(fieldName)) {
            objectNode.remove(fieldName)
        }
    }
}