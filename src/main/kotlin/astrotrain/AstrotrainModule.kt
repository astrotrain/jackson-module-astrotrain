package astrotrain

import com.fasterxml.jackson.core.Version
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.Module

class AstrotrainModuleBuilder() {
    private val mutators: MutableList<Pair<Class<*>, (JsonNode) -> Unit>> = mutableListOf()

    fun withMutatorFor(type: Class<*>, adapter: (JsonNode) -> Unit): AstrotrainModuleBuilder {
        mutators.add(type to adapter)
        return this
    }

    fun build(): AstrotrainModule = AstrotrainModule(mutators)
}

class AstrotrainModule(private val adapters: List<Pair<Class<*>, (JsonNode) -> Unit>>) : Module() {
    override fun getModuleName(): String = "astrotrain-adapter"

    override fun version(): Version = Version(1, 1, 1, "", "", "")
    override fun setupModule(context: SetupContext) {
        context.addBeanDeserializerModifier(AstrotrainBeanDeserializerModifier(adapters))
    }
}