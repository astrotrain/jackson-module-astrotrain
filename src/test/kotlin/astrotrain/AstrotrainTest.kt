package astrotrain

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class AstrotrainTest {

    data class Example(val field1: String, val field2: Int)

    private val astrotrainModuleBuilder = AstrotrainModuleBuilder()
            .withMutatorFor(Example::class.java, renameField("field4", "field3"))
            .withMutatorFor(Example::class.java, renameField("field3", "field2"))

    private val objectMapper: ObjectMapper = ObjectMapper()
            .registerModule(KotlinModule())
            .registerModule(astrotrainModuleBuilder.build())

    @Test
    fun `can deserialize example json`() {
        val expected = Example("a", 1)
        val json = objectMapper.writeValueAsString(expected)

        val actual = objectMapper.readValue<Example>(json, Example::class.java)

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `can handle field rename`() {
        val expected = Example("a", 1)
        val json = """ { "field1": "a", "field3": 1 } """
        val actual = objectMapper.readValue<Example>(json, Example::class.java)

        assertThat(actual).isEqualTo(expected)
    }

    @Test
    fun `can handle multiple renames and in the correct order`() {
        val expected = Example("a", 1)
        val json = """ { "field1": "a", "field4": 1 } """
        val actual = objectMapper.readValue<Example>(json, Example::class.java)

        assertThat(actual).isEqualTo(expected)
    }

}