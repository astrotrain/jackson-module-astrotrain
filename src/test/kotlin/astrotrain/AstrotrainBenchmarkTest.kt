package astrotrain

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.junit.Test
import kotlin.system.measureTimeMillis

class AstrotrainBenchmarkTest {

    data class Example(val field1: String, val field2: Int)

    @Test
    fun benchmark() {
        val objectMapper = objectMapper()
        val objectMapperWithAstrotrain = objectMapperWithAstrotrain()

        objectMapper.readValue<Example>(""" { "field1": "a", "field2": 1 } """, Example::class.java)
        val time1 = measureTimeMillis {
            for (x in 0..200) {
                objectMapper.readValue<Example>(""" { "field1": "a", "field2": $x } """, Example::class.java)
            }
        }
        val time2 = measureTimeMillis {
            for (x in 0..200) {
                objectMapperWithAstrotrain.readValue<Example>(""" { "field1": "a", "field4": $x } """, Example::class.java)
            }
        }

        println(time1)
        println(time2)
    }

    private fun objectMapper() : ObjectMapper {
        return ObjectMapper().registerModule(KotlinModule())
    }

    private fun objectMapperWithAstrotrain() : ObjectMapper {
        val astrotrainModuleBuilder = AstrotrainModuleBuilder()
                .withMutatorFor(Example::class.java, renameField("field4", "field3"))
                .withMutatorFor(Example::class.java, renameField("field3", "field2"))

        return ObjectMapper()
                .registerModule(KotlinModule())
                .registerModule(astrotrainModuleBuilder.build())
    }
}