plugins {
    id("org.jetbrains.kotlin.jvm") version "1.3.41"
    id("com.jfrog.bintray") version "1.8.4"
    id("maven-publish")
    `java-library`
}

version = "0.3"
group = "org.astrotrain"

buildscript {
    repositories {
        jcenter()
    }
}

repositories {
    jcenter()
}

bintray {
    user = "jozefdransfield"
    key = "c820342d8bcce5ad9d866623caa343aeba20bdd0"
    setPublications("maven")
    pkg.repo = "maven"
    pkg.name = "jackson-module-astrotrain"
    pkg.vcsUrl = "https://gitlab.com/astrotrain/jackson-module-astrotrain.git"
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            from(components["java"])
        }
    }
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.10.1")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.10.1")

    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.assertj:assertj-core:3.11.1")
    testImplementation("com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")
}

java {
    withSourcesJar()
    withJavadocJar()
}